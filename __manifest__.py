# -*- coding: utf-8 -*-
{
    'name': "Russia - localization",

    'summary': """
        Перевод российских областей от сообщества Open-odoo
        """,

    'description': """
        В модуле реалезовано:
        Перевод страны - Россия
        Имена регионов сделаны переводимыми 
        Перевод всех регионов России
        Добавлено 3 региона в Odoo (Забайкальский край, Республика Крым, Севастополь)
    """,

    'author': "Open Odoo",
    'website': "http://open-odoo.ru",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Localization',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'data/res.country.state.csv',
        # 'data/ir.translation.csv'
        # 'data/res_country_state_data.xml',
        

    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
    # 'post_init_hook': 'load_rus_translations',
}