# -*- coding: utf-8 -*-
import os
from odoo import api, SUPERUSER_ID, tools

# from . import controllers
from . import models
script_dir = os.path.dirname(os.path.abspath(__file__))

def load_rus_translations(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    tools.trans_load(cr, os.path.join(script_dir + '..' + 'i18n' + 'ru.po'), 'ru_RU', context={'overwrite': True})
    # env.ref('base.language.install').overwrite = True
    # env.ref('base.language.install').lang_install('ru_RU')
    
    # tools.trans_load_data(cr, filename, lang, verbose=True, module_name=None, context=None)
    # ru_RU